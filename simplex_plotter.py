#! /usr/bin/python -2.7-32
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt

class Simplex():
	def __init__(self, x1, x2, J, order):	
		length = len(x1)
		if any(len(list) != length for list in [x2, J, order]): 
			raise SimplexError(u"несоответствие количества точек")
			
		if len(x1) < 3:
			raise SimplexError(u"недостаточно точек")
	
		self.x1 = x1
		self.x2 = x2
		self.J = J
		self.order = order
		
	def solve(self):
		# решение
		solution = self.x1[self.order.index(0)], self.x2[self.order.index(0)]


		# треугольники
		ts = Simplex.triangles(self.x1, self.x2, self.order)
		# центры треугольников
		centers = [Simplex.center(t) for t in ts]

		# координаты треугольников и центров
		tsX1 = [[x[0] for x in t] for t in ts]
		tsX2 = [[x[1] for x in t] for t in ts]
		centersX1 = [p[0] for p in centers]
		centersX2 =  [p[1] for p in centers]

		# строим график
		fig, ax = plt.subplots(facecolor='w')

		# оси
		ax.set_xlim([min(self.x1) - 0.005, max(self.x1) + 0.01])
		ax.set_ylim([min(self.x2) - 0.005, max(self.x2) + 0.005])
		# ax.axis('equal')
		ax.set_xlabel('k1')
		ax.set_ylabel('k2')

		# точки и линии
		for i in range(len(ts)):
			tsX1[i].append(tsX1[i][0])
			tsX2[i].append(tsX2[i][0])
			ax.plot(tsX1[i], tsX2[i], '-', color='0.8')
		ax.plot(self.x1, self.x2, 'bs')
		ax.plot(centersX1, centersX2, 'g+')
		ax.plot(solution[0], solution[1], 'rs')

		# подписываем точки
		for i, (X, Y, J) in enumerate(zip(self.x1, self.x2, self.J)):
			# сколько раз были в точке, для смещения подписей
			count = 0
			for j in range(i):
				if self.x1[j] == X and self.x2[j] == Y:
					count += 1
			ax.annotate('{}'.format(J), xy=(X,Y), xytext=(0, 10), ha='center', textcoords='offset points', weight=700)
			ax.annotate('{}'.format(i+1), xy=(X,Y), xytext=(-5, -5 + -12*count), ha='right', textcoords='offset points')
			ax.annotate('{:.3}; {:.3}'.format(X,Y), xy=(X,Y), xytext=(5, -5), ha='left', textcoords='offset points', size='x-small')
			
		# подписываем центры
		for i, center in enumerate(centers):
			count = 0
			for j in range(i):
				if centers[j] == center:
					count += 1
					
			template = '{}'		
			if count > 0:	
				template = 	',{}'
			
			v_margin = 5
			if Simplex.is_stable_triangle(ts[i]):
				v_margin = -15
			ax.annotate(template.format(i+1), xy=(center[0], center[1]), xytext=(15*count, v_margin), ha='right', textcoords='offset points')	
			
		plt.show()
	
		
	@staticmethod
	def center(three_points):
		''' принимает список пар - точек треугольника
			возвращает пару-центр '''
		# в порядке возрастания x1
		x1gr = sorted(three_points, key=lambda point: point[0])
		
		if x1gr[0][1] == x1gr[2][1]:
			# горизонтальный треугольник
			x1 = x1gr[1][0]
			x2 = abs(x1gr[0][1] - x1gr[1][1])/2 + min(x1gr[0][1], x1gr[1][1])
			return (x1, x2)
		else:
			# вертикальный треугольник
			
			# в порядке возрастания x2
			x2gr = sorted(three_points, key=lambda point: point[1])

			x2 = x2gr[1][1]
			x1 = abs(x2gr[0][0] - x2gr[1][0])/2 + min(x2gr[0][0], x2gr[1][0])
			return (x1, x2)

	@staticmethod
	def is_stable_triangle(t):
		''' определяет ориентацию треугольника
			возвращает True, если треугольник "устойчив" (сторона, параллельная оси абсцисс, ближе к ней, чем треугольник)
		'''
		if t[0][1] == t[1][1]:
			return t[0][1] < t[2][1]
		if t[0][1] == t[2][1]:
			return t[0][1] < t[1][1]
		return t[1][1] < t[0][1]
		
	@staticmethod		
	def triangles(x, y, rotOrd):
		''' принимает списки координат точек и порядок обращения вершин симплекса 
			возвращает список треугольников, каждый треугольник - список пар с координатами его вершин '''
		length = len(x)
		if any(len(list) != length for list in [y, rotOrd]) or len(x) < 3: 
			raise SimplexError(u"не хватает точек для расчета треугольников")
			
		Ts = [[(x[0],y[0]), (x[1],y[1]), (x[2],y[2])]] # список треугольников	
		
		n = len(x) # кол-во точек
		for i in range(3, n):
			# осталось n-2 точек
			
			lastT = Ts[len(Ts)-1][:] # последний треугольник
			
			q = rotOrd.index(i-2)
			rotPoint = (x[q], y[q]) # поворачиваемая точка
			
			rpIndex = lastT.index(rotPoint) # индекс поворачиваемой точки в последнем треугольнике

			newT = lastT[:rpIndex] + lastT[(rpIndex + 1):]
			newT.append((x[i], y[i]))
			Ts.append(newT) # готовый треугольник добавляем в список
		return Ts
		
class SimplexError(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)		

if __name__ == '__main__':	
	# точки
	x1 = [0.4087,0.3913,0.4,0.4174,0.4087,0.4261,0.4174,0.4,0.4087,0.4261,0.4174,0.4348,0.4261,0.4435,0.4348,0.4348]
	x2 = [0.172,0.172,0.157,0.157,0.142,0.142,0.127,0.127,0.112,0.112,0.097,0.097,0.082,0.082,0.067,0.097]

	# целевая функция
	J = [171,176,134,133,110,115,90,94,78,76,70,70,65,64,73,70]

	# порядок обращения
	order = [2,1,3,4,6,5,8,7,9,10,11,12,0,0,13,14]
	
	s = Simplex(x1,x2,J,order)
	s.solve()


