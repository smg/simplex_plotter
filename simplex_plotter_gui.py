#! /usr/bin/python -2.7-32
# -*- coding: utf-8 -*-

import wx
import simplex_plotter
import sys

class MyFrame(wx.Frame):
	def __init__(self, parent, title):
		wx.Frame.__init__(self, parent, title=title, size=(800,420))
		panel = MyPanel(self)
		self.Show(True)

class MyPanel(wx.Panel):
	def __init__(self, parent):
		wx.Panel.__init__(self, parent)

		# create some sizers
		mainSizer = wx.BoxSizer(wx.VERTICAL)
		grid = wx.GridBagSizer(hgap=5, vgap=5)

		# надписи
		self.help = wx.StaticText(self, label=u"Формат данных: \nчисла с запятыми через пробел или табуляцию; \nчисла с точками через запятую, пробел или табуляцию.")
		self.error = wx.TextCtrl(self, style=wx.TE_MULTILINE | wx.TE_READONLY)
		
		# ввод данных
		self.k1label = wx.StaticText(self, label=u"координаты k1:")
		grid.Add(self.k1label, pos=(0,0))
		self.k1data = wx.TextCtrl(self, value="0.4087,0.3913,0.4,0.4174", size=(500,50), style=wx.TE_MULTILINE)
		grid.Add(self.k1data, pos=(0,1))
		
		self.k2label = wx.StaticText(self, label=u"координаты k2:")
		grid.Add(self.k2label, pos=(1,0))
		self.k2data = wx.TextCtrl(self, value="0,172\t0,172\t0,157\t0,157", size=(500,50), style=wx.TE_MULTILINE)
		grid.Add(self.k2data, pos=(1,1))
		
		self.Jlabel = wx.StaticText(self, label=u"значения функции:")
		grid.Add(self.Jlabel, pos=(2,0))
		self.Jdata = wx.TextCtrl(self, value="171,176,134,133", size=(500,50), style=wx.TE_MULTILINE)
		grid.Add(self.Jdata, pos=(2,1))
		
		self.orderlabel = wx.StaticText(self, label=u"порядок обращения вершин симплекса:\nнеобращенным ставим 0")
		grid.Add(self.orderlabel, pos=(3,0))
		self.order_data = wx.TextCtrl(self, value="0 1 0 0", size=(500,50), style=wx.TE_MULTILINE)
		grid.Add(self.order_data, pos=(3,1))
		
		# кнопка
		self.button =wx.Button(self, label=u"Нарисовать")
		self.Bind(wx.EVT_BUTTON, self.OnClick,self.button)
		grid.Add(self.button, pos=(4,1))
		
		
		# добавляем виджеты
		mainSizer.Add(self.help, 0, wx.ALL, 5)
		mainSizer.Add(grid, 0, wx.ALL, 5)
		mainSizer.Add(self.error, 1, wx.EXPAND)
		self.SetSizerAndFit(mainSizer)

	@staticmethod
	def parse(delim_str, name):
		delims = [' ', '\t', ',']
		delim = None
	
		for i in range(len(delims)):
			try:
				delim_str.index(delims[i])
				delim = delims[i]
				break
			except:
				pass
		
		if delim == None:
			raise MyError(name + u" - неизвестный разделитель")
		
		if delim == ',': # в числах только точки
			return [float(x) for x in delim_str.split(delim)]
		else:
			return [float(x.replace(',','.')) for x in delim_str.split(delim)]

	def OnClick(self,event):
		try:
			k1 = MyPanel.parse(self.k1data.GetValue(), 'k1')
			k2 = MyPanel.parse(self.k2data.GetValue(), 'k2')
			J = MyPanel.parse(self.Jdata.GetValue(), u'функция')
			order = MyPanel.parse(self.order_data.GetValue(), u'порядок')
		except MyError as e:
			self.show_error(u"Ошибка в данных: " + e.value)
		except:
			self.show_error(u"Неожиданная ошибка: " + str(sys.exc_info()[0]))
		else:
			try:
				s = simplex_plotter.Simplex(k1, k2, J, order)
				s.solve()
			except simplex_plotter.SimplexError as e:
				self.show_error(u"Ошибка при рисовании симплекса: " + e.value)
			except:
				self.show_error(u"Неожиданная ошибка при рисовании симплекса: " + str(sys.exc_info()[0]))
			else:
				self.error.SetValue("")
			
	def show_error(self, msg):
		self.error.AppendText(msg+'\n')
		
class MyError(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)		
		
		
app = wx.App(False)  # Create a new app, don't redirect stdout/stderr to a window.
frame = MyFrame(None, u"Рисование движения правильного симплекса")
frame.Show()
app.MainLoop()
